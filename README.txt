Question 3 Overview: This program is a simple keyword finder that loads a file from your local workstation, and searches
the file for a set of given key words. My starting goal was to read Twitter RSS feeds from the web, parsing the RSS
and creating item objects for the RSS elements then then search. It was during into a worm hole after about an hour
or two, so I just started over with a simpler approach. 

Write a brief, informal summary of your experience with TDD. What are your impressions of TDD? 
Overall this assignment provided a avenue for getting hands on experience with TDD, and illustrate the benefits of this 
development approach. It illustrated that TDD aids with ensuring tests are developed and developed properly for all
source, and more defensive mechanisms would be put in place to ensure failures are handled properly.

Do you like the approach? Do you dislike it? 
Overall it was a fun experience, but I neither liked or disliked the approach to development. I had mixed feelings about
the process. Although I found it easier the more I worked on this assignment.

What issues did you find with using TDD?
One issue that I found that I was continually re-factoring my code based upon the case study.Another issue was that it 
was more difficult for me to design the software with TDD, because do not have much experience with developing from a 
TDD perspective. For instance after I created a set of test cases, and then started development it was difficult for me
to hit all of the test cases, and I would then re-factor to hit all test cases I developed previously. Also I noticed 
not only re-factoring the source code but also the test code itself.

Would you use TDD in your development workflow?
Personally I would not. I could see where it holds value for experienced and specialized developers, but others I would
think TDD would add unneeded complexity.Although I think the unneeded complexity would add better test code coverage. 

Overall:
I enjoyed this project it was fun, and it is nice to have projects that are hands on. Also it was nice to get exposure to
GIT because I have not worked with it before. It was a challenge to not only get used to TDD but also GIT, but an enjoyable
and valuable experience. 