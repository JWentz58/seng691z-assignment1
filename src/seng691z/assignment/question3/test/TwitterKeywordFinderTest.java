package seng691z.assignment.question3.test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import seng691z.assignment.question3.TwitterReader;

public class TwitterKeywordFinderTest {
	//Test Data
	String espnTwitterFeed="C:\\Users\\Jacob\\"
			+ "Documents\\seng691z-assignment1\\SENG691Z-TDD"
			+ "\\Assignment1-TDD\\TestFiles\\ESPNTwitterRSS";
	String feedWithNoSports ="C:\\Users\\Jacob\\"
			+ "Documents\\seng691z-assignment1\\SENG691Z-TDD"
			+ "\\Assignment1-TDD\\TestFiles\\FooTwitterFeed";
	ArrayList sportsTopics = new ArrayList();
	ArrayList invalidTopics = new ArrayList();
	
	 @Before
	 public void init(){
		 sportsTopics.add("basketball");
		 sportsTopics.add("nfl");
		 sportsTopics.add("nba");
		 sportsTopics.add("super bowl");
		 sportsTopics.add("phoenix");
		 
		 invalidTopics.add("foo");
		 invalidTopics.add("foobar");
	 }
	

	@Test
	public void testValidLoad() {
		TwitterReader reader = new TwitterReader(sportsTopics);
		//Tests Loading Of Invalid Sources
		boolean loaded = reader.load(espnTwitterFeed);
		assertTrue(loaded);
		reader = null;
	}
	
	@Test
	public void testInvalidLoad() {
		TwitterReader reader = new TwitterReader(sportsTopics);
		//Tests Loading Of Invalid Sources
		boolean loaded = reader.load("foo bar");
		assertFalse(loaded);
		reader = null;
	}
	
	@Test
	public void testSearchForTopicsValid(){
		TwitterReader reader = new TwitterReader(sportsTopics);
		//Tests Loading Of Invalid Sources
		boolean loaded = reader.load(espnTwitterFeed);
		assertTrue(loaded);
		boolean topicsFound = reader.search();
		assertTrue(topicsFound);
	}
	
	@Test
	public void testSearchForTopicsInValid(){
		TwitterReader reader = new TwitterReader(invalidTopics);
		//Tests Loading Of Invalid Sources
		boolean loaded = reader.load(espnTwitterFeed);
		assertTrue(loaded);
		boolean topicsFound = reader.search();
		assertFalse(topicsFound);
	}

}
