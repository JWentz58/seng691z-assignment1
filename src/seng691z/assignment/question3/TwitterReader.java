package seng691z.assignment.question3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class TwitterReader {

	ArrayList<String> topics;
	String data;
	public TwitterReader(ArrayList topics) {
		this.topics = topics;
	}
	public boolean load(String path) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            sb.append(System.lineSeparator());
	            line = br.readLine();;
	        }
	        data = sb.toString();
	        data = data.toLowerCase();
		} catch (IOException e) {
			return false;
		}
		
		return true;
	}
	public boolean search() {
		if(data == null){
			return false;
		}
		for(String topic : topics){
			if(data.contains(topic)){
				return true;
			}
		}
		return false;
	}
	
	
	
}
