package seng691z.assignment.question1.test;

import static org.junit.Assert.*;

import org.junit.Test;

import seng691z.assignment.question1.FibonacciLookup;


public class FibonacciSeriesTest {

	@Test
	public void test() {
		
		long input[] ={1,2,3,4,12,20,29,-1};
		String output[] = {"0","1","1","2","89","4181","317811","Error"};
		FibonacciLookup fibLookup = new FibonacciLookup();
		assertNotNull(fibLookup);		
		for(int in=0;in<input.length;in++){
			String o = fibLookup.fibStart(input[in]-1);
			assertEquals(o,output[in]);
		}
	}
}
