package seng691z.assignment.question2;

public class TicTacToeBoard {
	private char[][] board; 
	GameState state;
	
	public TicTacToeBoard(){
		 board = new char[3][3];
		 initializeBoard();
		 state= GameState.EMPTY;
	}
	
	public void initializeBoard() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				board[i][j] = '-';
			}
		}	
	}
	
	public void draw()  {
		System.out.println("-----------");   
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if(j == 2)
					System.out.print(board[i][j] + "  ");
				else
					System.out.print(board[i][j] + " | ");
			}
			System.out.println();
			System.out.println("-----------");
		}
	}
	
	public char[][] getTicTacToeBoard(){
		return board;
	}
	
	public boolean placeIcon(TicTacToePiece piece){
		state= GameState.PLAYING;
		if(piece.xLoc>3 || piece.yLoc>3){
			return false;
		}
		else if(piece.xLoc<0 || piece.yLoc<0){
			return false;
		}
		else if(board[piece.xLoc][piece.yLoc] != '-'){
			return false;
		}
		else{
			board[piece.xLoc][piece.yLoc]=piece.icon;
		}
		draw();
		return true;
	}
	
	public boolean checkStateState(){
		for (int i = 0; i < 3; i++) {
			if((board[i][0] == board[i][1]) && (board[i][0]== board[i][2]) &&(board[i][2] ==board[i][1]) && board[i][0] != '-'){
				state= GameState.WON;
				return true;
			}
		}
		
		for (int i = 0; i < 3; i++) {
			if((board[0][i] == board[1][i]) && (board[0][i]== board[2][i]) &&(board[2][i] ==board[1][i])&& board[0][i] != '-'){
				state= GameState.WON;
				return true;
			}
		}
		
		if((board[1][1] ==board[0][0]) &&(board[1][1] ==board[2][2]) &&(board[0][0] ==board[2][2])&& board[0][0] != '-'){
			state= GameState.WON;
			return true;
		}
		
		if((board[1][1] ==board[0][2]) &&(board[1][1] ==board[2][0]) &&(board[2][0] ==board[0][2])&& board[2][0] != '-'){
			state= GameState.WON;
			return true;
		}
		return checkDraw();	
	}
	
	
	
	private boolean checkDraw() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if(board[i][j] == '-'){
					return false;
				}
			}
		}
		state= GameState.DRAW;
		return true;
	}



	public enum GameState {
	    EMPTY,NOT_READY,READY,PLAYING,WON, DRAW
	}

}
