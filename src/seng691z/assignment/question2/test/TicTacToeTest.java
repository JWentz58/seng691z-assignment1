package seng691z.assignment.question2.test;

import static org.junit.Assert.*;

import org.junit.Test;

import seng691z.assignment.question2.Player;
import seng691z.assignment.question2.TicTacToeBoard.GameState;
import seng691z.assignment.question2.TicTacToeGame;
import seng691z.assignment.question2.TicTacToePiece;


public class TicTacToeTest {
	
	
	
	@Test
	public void testGameObjectValid(){
		System.out.println("===== TEST START: Valid Object ====");
		TicTacToeGame game = new TicTacToeGame();
		//Asserts that the object for controlling the TicTacToe Game is not null
		/*======================================================
			Test Draw Board
			-- Asserts Game Object is not null
			-- Draws Board makes makes sure the Game State is correct
			-- Attempts to Start Game without Players allocated
		=========================================================*/		
		assertNotNull(game);
		assertEquals(game.getBoardState(),GameState.EMPTY);
		// Add User To Game
		//Test Starting game with no players added
		assertEquals(game.startGame(),GameState.NOT_READY);
		System.out.println("===== TEST END: Valid Object ====");

	}
	
	@Test
	public void testStartOfGame(){
		System.out.println("===== TEST START: Test Start Of Game ====");
		TicTacToeGame game = new TicTacToeGame();
		String player1Name="Jacob";
		String player2Name="Jim";
		//Asserts that the object for controlling the TicTacToe Game is not null
		/*======================================================
			Test Draw Board
			-- Asserts Game Object is not null
			-- Draws Board makes makes sure the Game State is correct
			-- Attempts to Start Game without Players allocated
		=========================================================*/		
		//Test Starting game with one player added
		Player player1 = new Player(player1Name);
		game.setPlayer1(player1);
		assertEquals(game.startGame(),GameState.NOT_READY);
		//Ensure Starting of Game can Occur
		Player player2 = new Player(player2Name);
		game.setPlayer2(player2);
		//Place Icons
		//Test placing of Icons outside the bounds of the board
		assertEquals(game.startGame(),GameState.PLAYING);
		System.out.println("===== TEST END: Test Start Of Game ====");
	}
	
	@Test
	public void testPiecePlacement(){
		System.out.println("===== TEST START: Test Place Piece ====");
		TicTacToeGame game = new TicTacToeGame();
		String player1Name="Jacob";
		String player2Name="Jim";
		//Asserts that the object for controlling the TicTacToe Game is not null
		/*======================================================
			Test Draw Board
			-- Asserts Game Object is not null
			-- Draws Board makes makes sure the Game State is correct
			-- Attempts to Start Game without Players allocated
		=========================================================*/		
		//Test Starting game with one player added
		game.setPlayer1(new Player(player1Name));
		game.setPlayer2(new Player(player2Name));
		// Test Invalid Piece Icon 
		assertFalse(game.insertPiece(new TicTacToePiece('i',1,1)));
		//Test Turn
		assertFalse(game.insertPiece(new TicTacToePiece('o',1,1)));
		// Have Player 1 attempt to add piece when it is not his turn
		assertTrue(game.insertPiece(new TicTacToePiece('x',1,1)));
		assertFalse(game.insertPiece( new TicTacToePiece('x',0,0)));
		assertFalse(game.insertPiece(new TicTacToePiece('x',0,0)));
		
		
		assertFalse(game.insertPiece(new TicTacToePiece('x',1,1)));
		assertFalse(game.insertPiece(new TicTacToePiece('x',1,1)));
		
		// Test Invalid Piece Location 
		assertFalse(game.insertPiece(new TicTacToePiece('x',4,4)));
		assertFalse(game.insertPiece(new TicTacToePiece('x',4,4)));
		System.out.println("===== TEST END: Test Place Piece ====");
	}
	
	@Test
	public void testClearBoard(){
		System.out.println("===== TEST START: Clear Board ====");
		TicTacToeGame game = new TicTacToeGame();
		String player1Name="Jacob";
		String player2Name="Jim";
		//Asserts that the object for controlling the TicTacToe Game is not null
		/*======================================================
			Test Draw Board
			-- Asserts Game Object is not null
			-- Draws Board makes makes sure the Game State is correct
			-- Attempts to Start Game without Players allocated
		=========================================================*/		
		//Test Starting game with one player added
		game.setPlayer1(new Player(player1Name));
		game.setPlayer2(new Player(player2Name));
		/*======================================================
			Test Clear Board
			-- Adds Three Icons
			-- Clear Boards (Starts New Game)
			-- Places Icons in Same Location of the previous three Icons
		=========================================================*/
		//Test Clear Board Placing three Icons
		assertTrue(game.insertPiece(new TicTacToePiece('x',1,1)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',1,0)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',2,2)));
		game.clearBoard();
		//Attempt to Place the Same Icons
		assertTrue(game.insertPiece(new TicTacToePiece('x',1,1)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',1,0)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',2,2)));
		System.out.println("===== TEST END: Clear Board ====");
	}
	
	public void testWinConditionHori(){
		    System.out.println("===== TEST START: Test Hori Win ====");
			TicTacToeGame game = new TicTacToeGame();
			String player1Name="Jacob";
			String player2Name="Jim";
			Player player1 = new Player(player1Name);
			Player player2 = new Player(player2Name);
			game.setPlayer1(player1);
			game.setPlayer2(player2);
			/*======================================================
			Test Win and Draw
			-- Tests Invalid Icon Placement
			-- Tests to ensure it is the players turn who is placing Icons
			-- Tests bounds to ensure location of piece is valid
			=========================================================*/
			//Test Across
			game.clearBoard();
			assertTrue(game.insertPiece(new TicTacToePiece('x',1,1)));
			assertTrue(game.insertPiece(new TicTacToePiece('o',2,2)));
			assertTrue(game.insertPiece(new TicTacToePiece('x',1,2)));
			assertTrue(game.insertPiece(new TicTacToePiece('o',2,1)));
			assertTrue(game.insertPiece(new TicTacToePiece('x',1,0)));
			assertEquals(game.getWinner(),player1);
			//Test Accross
			game.startGame();
			assertTrue(game.insertPiece(new TicTacToePiece('x',1,1)));
			assertTrue(game.insertPiece(new TicTacToePiece('o',2,2)));
			assertTrue(game.insertPiece(new TicTacToePiece('x',1,2)));
			assertTrue(game.insertPiece(new TicTacToePiece('o',2,1)));
			assertTrue(game.insertPiece(new TicTacToePiece('x',0,0)));
			assertTrue(game.insertPiece(new TicTacToePiece('o',2,0)));
			assertEquals(game.getWinner(),player2);
			 System.out.println("===== TEST END: Test Hori Win ====");
	}
	
	@Test
	public void testWinConditionVert(){
		System.out.println("===== TEST START: Test Vert Win ====");
		TicTacToeGame game = new TicTacToeGame();
		String player1Name="Jacob";
		String player2Name="Jim";
		Player player1 = new Player(player1Name);
		Player player2 = new Player(player2Name);
		game.setPlayer1(player1);
		game.setPlayer2(player2);
		/*======================================================
		Test Win and Draw
		-- Tests Invalid Icon Placement
		-- Tests to ensure it is the players turn who is placing Icons
		-- Tests bounds to ensure location of piece is valid
		=========================================================*/
		//Test Verticial 
		game.startGame();
		assertTrue(game.insertPiece(new TicTacToePiece('x',0,0)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',2,2)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',1,0)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',2,1)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',2,0)));
		assertEquals(game.getWinner(),player1);
		//Test Ending conditions
		System.out.println("===== TEST END: Test Vert Win ====");
	}
	
		
	@Test
	public void testWinConditionDiag(){
		System.out.println("===== TEST START: Test Diag Win ====");
		TicTacToeGame game = new TicTacToeGame();
		String player1Name="Jacob";
		String player2Name="Jim";
		Player player1 = new Player(player1Name);
		Player player2 = new Player(player2Name);
		game.setPlayer1(player1);
		game.setPlayer2(player2);
		game.startGame();
		assertTrue(game.insertPiece(new TicTacToePiece('x',0,0)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',0,2)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',1,1)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',2,1)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',2,2)));
		assertEquals(game.getWinner(),player1);
		System.out.println("===== TEST END: Test Diag Win ====");
	}
	
	@Test
	public void testWinConditionDraw(){
		System.out.println("===== TEST START: Test DRAW ====");
		TicTacToeGame game = new TicTacToeGame();
		String player1Name="Jacob";
		String player2Name="Jim";
		Player player1 = new Player(player1Name);
		Player player2 = new Player(player2Name);
		game.setPlayer1(player1);
		game.setPlayer2(player2);
		game.startGame();
		assertTrue(game.insertPiece(new TicTacToePiece('x',0,0)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',0,1)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',0,2)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',1,0)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',1,1)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',2,0)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',1,2)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',2,2)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',2,1)));
		System.out.println("===== TEST END: Test DRAW ====");
	}
	

	
	
/*
	@Test
	public void test() {
		TicTacToeGame game = new TicTacToeGame();
		String player1Name="Jacob";
		String player2Name="Jim";
		//Asserts that the object for controlling the TicTacToe Game is not null
		assertNotNull(game);
		//Asserts Game State is correct
		assertEquals(game.getBoardState(),GameState.EMPTY);
		// Add User To Game
		//Test Starting game with no players added
		assertEquals(game.startGame(),GameState.NOT_READY);
		//Test Starting game with one player added
		Player player1 = new Player(player1Name);
		game.setPlayer1(player1);
		assertEquals(game.startGame(),GameState.NOT_READY);
		//Ensure Starting of Game can Occur
		Player player2 = new Player(player2Name);
		game.setPlayer2(player2);
		//Place Icons
		//Test placing of Icons outside the bounds of the board
		assertEquals(game.startGame(),GameState.PLAYING);
		//Test Clear Board Placing three Icons
		assertTrue(game.insertPiece(new TicTacToePiece('x',1,1)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',1,0)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',2,2)));
		game.clearBoard();
		//Attempt to Place the Same Icons
		assertTrue(game.insertPiece(new TicTacToePiece('x',1,1)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',1,0)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',2,2)));
		

		game.clearBoard();
		// Test Invalid Piece Icon 
		assertFalse(game.insertPiece(new TicTacToePiece('i',1,1)));
		//Test Turn
		assertFalse(game.insertPiece(new TicTacToePiece('o',1,1)));
		// Have Player 1 attempt to add piece when it is not his turn
		assertTrue(game.insertPiece(new TicTacToePiece('x',1,1)));
		assertFalse(game.insertPiece( new TicTacToePiece('x',0,0)));
		assertFalse(game.insertPiece(new TicTacToePiece('x',0,0)));
		
		
		assertFalse(game.insertPiece(new TicTacToePiece('x',1,1)));
		assertFalse(game.insertPiece(new TicTacToePiece('x',1,1)));
		
		// Test Invalid Piece Location 
		assertFalse(game.insertPiece(new TicTacToePiece('x',4,4)));
		assertFalse(game.insertPiece(new TicTacToePiece('x',4,4)));
		
		//Test Across
		game.clearBoard();
		assertTrue(game.insertPiece(new TicTacToePiece('x',1,1)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',2,2)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',1,2)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',2,1)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',1,0)));
		assertEquals(game.getWinner(),player1);
		//Test Accross
		game.startGame();
		assertTrue(game.insertPiece(new TicTacToePiece('x',1,1)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',2,2)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',1,2)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',2,1)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',0,0)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',2,0)));
		assertEquals(game.getWinner(),player2);
		//Test Verticial 
		game.startGame();
		assertTrue(game.insertPiece(new TicTacToePiece('x',0,0)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',2,2)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',1,0)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',2,1)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',2,0)));
		assertEquals(game.getWinner(),player1);
		//Test Ending conditions
		
		//Test Diag
		game.startGame();
		assertTrue(game.insertPiece(new TicTacToePiece('x',0,0)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',0,2)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',1,1)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',2,1)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',2,2)));
		assertEquals(game.getWinner(),player1);
		
		//Test Draw
		game.startGame();
		assertTrue(game.insertPiece(new TicTacToePiece('x',0,0)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',0,1)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',0,2)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',1,0)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',1,1)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',2,0)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',1,2)));
		assertTrue(game.insertPiece(new TicTacToePiece('o',2,2)));
		assertTrue(game.insertPiece(new TicTacToePiece('x',2,1)));

		
	}
*/

}
