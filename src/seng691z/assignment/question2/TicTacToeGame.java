package seng691z.assignment.question2;

import seng691z.assignment.question2.TicTacToeBoard.GameState;


public class TicTacToeGame {
	
	TicTacToeBoard board;
	Player player1;
	Player player2;
	GameState gameState;
	boolean gameOver = false;
	public TicTacToeGame(){
		board = new TicTacToeBoard();
	}
	
	public GameState startGame(){
		if(player1 == null || player2 == null){
			return GameState.NOT_READY;
		}
		clearBoard();
		player1.setWinnerFlag(false);
		player2.setWinnerFlag(false);
		gameState= GameState.PLAYING;
		return gameState;
	}
	
	public void clearBoard(){
		System.out.println("::New Game::");
		board = new TicTacToeBoard();
		this.player1.setTurn(true);
		this.player2.setTurn(false);
		drawBoard();
	}
	
	public boolean insertPiece(TicTacToePiece piece){
		boolean flag = false;
		if(gameOver==true){
			System.out.println("Please Start New Game");
			return false;
		}
		if(piece.icon == player1.getIcon() && player1.getTurn() == true){
			flag= board.placeIcon(piece);
			gameOver=board.checkStateState();
			if(gameOver==true){
				player1.setWinnerFlag(true);
			}
		}
		else if(piece.icon == player2.getIcon() && player2.getTurn() == true){
			flag = board.placeIcon(piece);
			gameOver=board.checkStateState();
			if(gameOver==true){
				player2.setWinnerFlag(true);
			}
		}
		else{
			return false;
		}
		if(player1.getTurn()==true){
			player1.setTurn(false);
			player2.setTurn(true);
		}
		else if(player2.getTurn()==true){
			player1.setTurn(true);
			player2.setTurn(false);		
		}
		
		if(gameOver==true){
			System.out.println(getGameWinner());
			gameOver=false;
		}
		return flag;
	}
	
	
	public void drawBoard(){
		board.draw();
	}
	
	public GameState getBoardState(){
		return board.state;
	}
	
	public void setPlayer1(Player player1){
		this.player1=player1;
		this.player1.setIcon('x');
		this.player1.setTurn(true);
	}
	
	public void setPlayer2(Player player2){
		this.player2=player2;
		this.player2.setIcon('o');
	}
	
	public  Player getWinner(){
		if(player1.isWinnerFlag())
			return player1;
		
		if(player2.isWinnerFlag())
			return player2;
		
		return null;
	}
	
	public String getGameWinner(){
		if(board.state == GameState.WON){
			if(player1.isWinnerFlag()==true)
				return "Game Over: Player :"+player1.getPlayerName() + " Wins!";
			if(player2.isWinnerFlag()==true)
				return  "Game Over: Player: "+player2.getPlayerName()+ " Wins!";
		}
		if(board.state == GameState.DRAW){
			return "Game Over: Draw";
		}
		return  "ERROR";
		
	}
	
}
