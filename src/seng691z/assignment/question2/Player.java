package seng691z.assignment.question2;

import java.util.ArrayList;

public class Player {
	private String playerName;
	ArrayList<TicTacToePiece> pieces;
	private boolean turn = false;
	private char icon;
	private boolean winnerFlag;
	public Player(String playerName){
		this.setPlayerName(playerName);
		 pieces= new ArrayList();
	}
	
	public void addPiece(TicTacToePiece piece){
		pieces.add(piece);
	}
	
	public ArrayList getPiece(){
		return pieces;
	}
	
	public void setIcon(char icon){
		this.icon=icon;
	}
	
	public char getIcon(){
		return icon;
	}
	
	public void setTurn(boolean turn){
		this.turn=turn;
	}
	
	public boolean getTurn(){
		return turn;
	}

	public boolean isWinnerFlag() {
		return winnerFlag;
	}

	public void setWinnerFlag(boolean winnerFlag) {
		this.winnerFlag = winnerFlag;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	
	
	
}
